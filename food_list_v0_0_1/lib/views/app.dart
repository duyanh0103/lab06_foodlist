import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/pizza.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'platform_alert.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food App',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // String pizzaString = '';
  late int appCounter;
  late int selectedIndexBottomMenu; //build bottom menu

  @override
  void initState() {
    readAndWritePreferences(); //minh họa đọc ghi dữ liệu
    readJsonFile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Homepage'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {},
      ),
      body: Container(
        child: FutureBuilder(
          future: readJsonFile(),
          builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
            return ListView.builder(
                itemCount: pizzas.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  var itemData = pizzas.data![index];
                  return ListTile(
                    title: Text(itemData.pizzaName),
                    subtitle:
                        Text("${itemData.description} \$ ${itemData.price}"),
                  );
                });
          },
        ),
      ),
      bottomNavigationBar: _buildBottomMenu(),
    );
  }

  Future<List<Pizza>> readJsonFile() async {
    String myString = await DefaultAssetBundle.of(context)
        .loadString('assets/pizza_list.json');

    List myMap = jsonDecode(myString);
    List<Pizza> myPizzas = [];
    myMap.forEach((dynamic pizza) {
      Pizza myPizza = Pizza.fromJson(pizza);
      myPizzas.add(myPizza);
    });

    return myPizzas;
  }

  Future readAndWritePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    //lấy dữ liệu nếu ko thì bắt đầu là 0
    appCounter = prefs.getInt('appCounter') ?? 0;
    appCounter++;

    await prefs.setInt('appCounter', appCounter);
    setState(() {
      //nhét setInt vô cũng đc
    });
  }

  BottomNavigationBar _buildBottomMenu() {
    return BottomNavigationBar(
      items: [
        //item
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
        BottomNavigationBarItem(
            icon: Icon(Icons.settings), label: 'Preferences'),
      ],
      onTap: _onBottomMenuTap,
    );
  }

  void _onBottomMenuTap(int value) {
    setState(() {
      selectedIndexBottomMenu = value;
    });
    if (value == 1) {
      final alert = PlatformAlert(
          title: 'Time of opened app',
          message: 'You have opened the app is $appCounter times.'
      );
      alert.show(context);
    }
  }
}
