import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class PlatformAlert {
  final String? title;
  final String? message;
  //tạo constructor/ để cho nó an toàn và tránh TH cho nó bị null
  const PlatformAlert({@required this.title, @required this.message})
      : assert(title != null),
        assert(message != null);
  //viết show function lấy theme từ context của platform
  void show(BuildContext context) {
    final platform = Theme.of(context).platform;

    //check app chạy trên platform nào:
    if (platform == TargetPlatform.iOS) {
      _buildCupertinoAlert(context);
    } else {
      _buildMaterialAlert(context);
    }
  }

  //method cho 2 cái build platform
  void _buildCupertinoAlert(BuildContext context) {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title!),
            content: Text(message!),
            actions: [
              CupertinoButton(
                  child: Text("Close"),
                  onPressed: () => Navigator.of(context).pop())
            ],
          );
        });
  }

  void _buildMaterialAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title!),
            content: Text(message!),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Close'))
            ],
          );
        });
  }
}
